import numpy as np
import matplotlib.pyplot as plt

fading = np.loadtxt("BPSK_Fading.txt")
awgn = np.loadtxt("BPSK_AWGN.txt")

plt.semilogy(fading[:,0],fading[:,1], label='Fading')
plt.semilogy(awgn[:,0],awgn[:,1], label='AWGN')
plt.xlabel('SNR in dB')
plt.ylabel('Bit Error Rate')
plt.title('BER vs SNR for BPSK: Fading vs AWGN')
plt.grid()
plt.legend()
plt.show()