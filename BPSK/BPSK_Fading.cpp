// BPSK over Fading Channel

#include <itpp/itcomm.h>
#include <fstream>
using namespace itpp;
using std::cout;
using std::endl;
int main()
{
	std::ofstream out("BPSK_Fading.txt");

	int i, Number_of_bits;
	double Ec;
	vec SNR_dB, SNR, N0, noise_variance, bit_error_rate; 
	bvec transmitted_bits, received_bits;                 
	cvec transmitted_symbols, received_symbols, rec_test, tx_test;
	cmat channel_coeffs;
	double norm_dopp = 0.1;			//f_d - Fading rate normalized to data rate
	//f_d = F_d * Ts 		F_d = fc * mu / c

	//Classes
	BPSK_c bpsk;  //The BPSK modulator/debodulator class - for complex symbols
	AWGN_Channel awgn_channel;     //The AWGN channel class 
	BERC berc;  //The Bit Error Rate Counter class
	TDL_Channel fading_channel;

	fading_channel.set_norm_doppler(norm_dopp);
	
	Ec = 1.0;                      //The transmitted energy per QAM symbol is 1.
	SNR_dB = linspace(-20, 20.0, 21); //SNR is energy per received complex symbol time by energy of received noise per complex symbol time 
	SNR = inv_dB(SNR_dB);         
	N0 = pow(SNR, -1.0);     
	Number_of_bits = 100008; 

	bit_error_rate.set_size(SNR_dB.length(), false);
	fading_channel.generate(Number_of_bits, channel_coeffs);
	RNG_randomize();
	for (i = 0; i < SNR_dB.length(); i++)
	{
		transmitted_bits = randb(Number_of_bits);
		transmitted_symbols = bpsk.modulate_bits(transmitted_bits);
		awgn_channel.set_noise(N0(i));

		tx_test = channel_coeffs * transmitted_symbols;
		
		for(int j = 0; j < Number_of_bits; j++)
		{
			tx_test(j) = channel_coeffs(j,0) * transmitted_symbols(j);
		}

		received_symbols = awgn_channel(tx_test);
		//rec_test = transpose(hermitian_transpose(channel_coeffs))*received_symbols;
		rec_test = ones_c(Number_of_bits);
		for(int j = 0; j < Number_of_bits; j++)
		{
			rec_test(j) = received_symbols(j) * conj(channel_coeffs(j,0));
		}

		received_bits = bpsk.demodulate_bits(rec_test);
		
		berc.clear();                               
		berc.count(transmitted_bits, received_bits);
		bit_error_rate(i) = berc.get_errorrate();   
	}

	//Print the results:
	cout << "SNR_dB = " << SNR_dB << " [dB]" << endl;
	cout << "BER = " << bit_error_rate << endl;
	cout << "Saving results to BPSK_Fading.txt" << endl;
	cout << endl;

	for (i = 0; i < SNR_dB.length(); i++)
	{
		out << SNR_dB[i] << "\t";
		out << bit_error_rate[i] << "\t";
		out << inv_dB(SNR_dB[i]) << endl;
	}
	out.close();

	return 0;
}