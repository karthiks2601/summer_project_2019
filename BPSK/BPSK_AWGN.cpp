// BPSK over Fading Channel

#include <itpp/itcomm.h>
#include <fstream>
using namespace itpp;
using std::cout;
using std::endl;
int main()
{
	std::ofstream out("BPSK_AWGN.txt");

	int i, Number_of_bits;
	double Ec;
	vec SNR_dB, SNR, N0, noise_variance, bit_error_rate; 
	bvec transmitted_bits, received_bits;                 
	cvec transmitted_symbols, received_symbols, rec_test, tx_test;
	
	//Classes
	BPSK_c bpsk;  //The BPSK modulator/debodulator class - for complex symbols
	AWGN_Channel awgn_channel;     //The AWGN channel class 
	BERC berc;  //The Bit Error Rate Counter class
	

	
	Ec = 1.0;                      //The transmitted energy per QAM symbol is 1.
	SNR_dB = linspace(-20, 20.0, 21); //SNR is energy per received complex symbol time by energy of received noise per complex symbol time 
	SNR = inv_dB(SNR_dB);         
	N0 = pow(SNR, -1.0);     
	Number_of_bits = 100008; 

	bit_error_rate.set_size(SNR_dB.length(), false);
	RNG_randomize();
	for (i = 0; i < SNR_dB.length(); i++)
	{
		transmitted_bits = randb(Number_of_bits);
		transmitted_symbols = bpsk.modulate_bits(transmitted_bits);
		awgn_channel.set_noise(N0(i));

		received_symbols = awgn_channel(transmitted_symbols);
		
		received_bits = bpsk.demodulate_bits(received_symbols);
		
		berc.clear();                               
		berc.count(transmitted_bits, received_bits);
		bit_error_rate(i) = berc.get_errorrate();   
	}

	//Print the results:
	cout << "SNR_dB = " << SNR_dB << " [dB]" << endl;
	cout << "BER = " << bit_error_rate << endl;
	cout << "Saving results to BPSK_AWGN.txt" << endl;
	cout << endl;

	for (i = 0; i < SNR_dB.length(); i++)
	{
		out << SNR_dB[i] << "\t";
		out << bit_error_rate[i] << "\t";
		out << inv_dB(SNR_dB[i]) << endl;
	}
	out.close();

	return 0;
}