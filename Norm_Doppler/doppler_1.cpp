// Rayleigh Fading Channel

#include <itpp/itcomm.h>
#include <fstream>
using namespace itpp;
using std::cout;
using std::endl;

int main()
{
	std::ofstream out("Norm_Doppler_BER.txt");

	int Number_of_bits, i, j, pilot_length;
	double Ec, norm_dopp;
	double SNR_dB, SNR, N0, noise_variance; 
	bvec transmitted_bits, received_bits;                 
	cvec transmitted_symbols, received_symbols, rec_eq, hx, channel_estimate;
	cmat channel_coeffs;
	ivec block_length;
	vec bit_error_rate;

	//Classes
	BPSK_c bpsk;  //The BPSK modulator/debodulator class - for complex symbols
	AWGN_Channel awgn_channel;     //The AWGN channel class 
	BERC berc;  //The Bit Error Rate Counter class
	TDL_Channel fading_channel;

	Ec = 1.0;
	SNR_dB = 1; //SNR is energy per received complex symbol time by energy of received noise per complex symbol time 
	SNR = inv_dB(SNR_dB);
	N0 = pow(SNR, -1.0);
	
	Number_of_bits = 50000;
	norm_dopp = 0.001;

	RNG_randomize();
	fading_channel.set_norm_doppler(norm_dopp);
	fading_channel.generate(Number_of_bits, channel_coeffs);
	
	//------------------------------------------------------------------
	// Estimating the channel
	// Transmit all ones (bits = 0) and estimate h for block length block_length and
	// pilot length pilot_length.
	pilot_length = 5;	
	block_length = linspace_fixed_step(10, 1000, 5);
	bit_error_rate.set_size(block_length.length(), false);
	for(i = 0; i < block_length.length(); i++)
	{
		
		//------------------------------------------------------------------
		// Transmit the data and find BER for the particular block length.
		transmitted_bits = randb(Number_of_bits);
		transmitted_symbols = bpsk.modulate_bits(transmitted_bits);
		for(j = 0; j < int(Number_of_bits/block_length(i)); j++)
		{
			for(int k = 0; k < pilot_length; k++)
			{
				transmitted_symbols[block_length(i)*j + k] = 1;
			}
		}
		awgn_channel.set_noise(N0);
		hx = channel_coeffs * transmitted_symbols;
		for(j = 0; j < Number_of_bits; j++)
		{
			hx(j) = channel_coeffs(j,0) * transmitted_symbols(j);
		}
		received_symbols = awgn_channel(hx);


		channel_estimate = zeros_c(Number_of_bits);
		for(j = 0; j < int(Number_of_bits/block_length(i)); j++)
		{
			for(int k = 0; k < pilot_length; k++)
			{
				channel_estimate(block_length(i)*j) += received_symbols(block_length(i)*j + k);
			}
			channel_estimate(block_length(i)*j) = channel_estimate(block_length(i)*j)/pilot_length;
		}
		for(j = 0; j < Number_of_bits; j++)
		{
			if(j%block_length(i) != 0)
			{
				channel_estimate(j) = channel_estimate(j-1);
			}
		}

		rec_eq = ones_c(Number_of_bits);
		for(j = 0; j < Number_of_bits; j++)
		{
			rec_eq(j) = received_symbols(j) * pow(channel_estimate(j), -1);
		}
		transmitted_bits = bpsk.demodulate_bits(transmitted_symbols); // Since transmitted symbols have been modified
		//																so as to include the pilot symbols.
		received_bits = bpsk.demodulate_bits(rec_eq);
		
		berc.clear();                               
		berc.count(transmitted_bits, received_bits);
		bit_error_rate(i) = berc.get_errorrate();
	}
	
	
	
	//------------------------------------------------------------------
	cout << "Norm Doppler = " << norm_dopp << endl;
	cout << block_length << endl;
	cout << bit_error_rate << endl;	
	for(i = 0; i < block_length.length(); i++)
	{
		out << block_length(i) << "\t";
		out << bit_error_rate(i) << endl;
	}	
	out.close();	
	return 0;
}
