import numpy as np
import matplotlib.pyplot as plt

f = np.loadtxt("Norm_Doppler_BER.txt")

plt.plot(f[:,0],f[:,1])

plt.xlabel('Block Length')
plt.ylabel('Bit Error Rate')
plt.title('BER vs Block Length for Normalized Doppler = 0.001 with SNR = 10dB')
plt.grid(linestyle='dotted')
plt.axhline(y=2*f[0,1], linestyle = 'dotted', color='red')
plt.legend()
plt.show()