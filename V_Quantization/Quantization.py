# Geodesic Interpolation

import numpy as np
import matplotlib.pyplot as plt
import sys

out = open("Data.txt", "w+")

def randn_c(nRx, nTx):
	return ((np.random.randn(nRx, nTx) + 1j* np.random.randn(nRx, nTx)) / np.sqrt(2))

arguments = sys.argv[1:]
count = len(arguments) // 2
if (count == 0):
	print('Please input the Power Delay Profile as Command Line Arguments with odd entries ' \
			'indicating the index and even entries indicating the magnitude')
	exit()

index = np.ones(count)
magnitude = np.ones(count)
for i in range(count):
	index[i] = arguments[2*i]
	magnitude[i] = arguments[2*i + 1]

nTx = 4
nRx = 4
nFFT = 64

#----------------------------------------------------------------------
def main():
	omega = np.linspace(0, 2*np.pi, nFFT)

	H = np.zeros((nFFT, nRx, nTx)) + 1j * np.zeros((nFFT, nRx, nTx))
	U = np.zeros((nFFT, nRx, nTx)) + 1j * np.zeros((nFFT, nRx, nTx))
	Vh = np.zeros((nFFT, nRx, nTx)) + 1j * np.zeros((nFFT, nRx, nTx))
	V =  np.zeros((nFFT, nRx, nTx)) + 1j * np.zeros((nFFT, nRx, nTx)) 
	sigma = np.zeros((nFFT, nTx))

	for i in range(count):
		temp = randn_c(nRx, nTx)
		for k in range(nFFT):
			H[k] += temp * np.exp(-1j * index[i] * omega[k]) * pow(10, magnitude[i] / 10)
			
	for k in range(nFFT):
		U[k], sigma[k], Vh[k] = np.linalg.svd(H[k])
		V[k] = Vh[k].conj().T		# Hermitian - .H is not available for ndarray
		

	#----------------------------------------------------------------------
	# To find the Optimal Orientation Matrix
	Theta = np.zeros((nFFT - 1, nTx, nTx)) + 1j*np.zeros((nFFT - 1, nTx, nTx))		# Optimal Orientation Matrix is a diagonal matrix
	theta = np.zeros((nFFT - 1, nTx)) + 1j*np.zeros((nFFT - 1, nTx))		# theta contains the diagonal entries of Theta
	A = np.zeros((nFFT - 1, nRx, nTx)) + 1j * np.zeros((nFFT - 1, nRx, nTx))
	Delta = np.eye(nTx, nTx)
	S = np.zeros((nFFT, nTx, nTx)) + 1j * np.zeros((nFFT, nTx, nTx))
	S_real = np.zeros((nFFT, nTx, nTx))
	S_imag = np.zeros((nFFT, nTx, nTx))
	
	for k in range(nFFT - 1):
		A[k] = (V[k+1].conj().T) @ V[k]
		
		for j in range(nTx):
			theta[k][j] = A[k][j][j] / abs(A[k][j][j])

		Theta[k] = np.diag(theta[k])
		V[k+1] = V[k+1] @ Theta[k]
		#print(np.linalg.norm(V[k] - V[k+1], 'fro'))		# To confirm that multiplication with Theta decreases the norm
		#print(np.linalg.norm(V[k] - V[k+1] @ Theta[k], 'fro'))
#-----------------------------------------------------------------------
	for k in range(nFFT):
		S[k] = np.log(V[k])
		S_real[k] = np.real(S[k])
		S_imag[k] = np.imag(S[k])

	nSubcarrier = np.linspace(1, nFFT, nFFT)
	S_00_real_coeff_6 = np.polyfit(nSubcarrier, S_real[:, 0, 0], 6)	#Order 6 polynomial
	S_00_real_estimate_6 = np.poly1d(S_00_real_coeff_6) (nSubcarrier)
	S_00_real_coeff_4 = np.polyfit(nSubcarrier, S_real[:, 0, 0], 4)	#Order 4 polynomial
	S_00_real_estimate_4 = np.poly1d(S_00_real_coeff_4) (nSubcarrier)
	
	# Comparing Polynomial Approximations of real or imaginary part of an element of S matrix with the actual realization
	plt.figure(1)
	plt.plot(S_real[:, 0, 0], label='Actual')		# Plotting real or imaginary part of S[0][0] across subcarriers using slicing
	plt.plot(S_00_real_estimate_6, label='Degree 3 Polynomial Approximation')
	plt.plot(S_00_real_estimate_4, label='Degree 4 Polynomial Approximation')
	plt.grid(linestyle='dotted')
	plt.legend()
	plt.title('Real Part of S[0][0]')
	plt.show(block=False)
#-----------------------------------------------------------------------
	'''
	# Unwrapping imaginary part of S, i.e. phase of V
	S_00_real = np.unwrap(S_00_real)	
	Subcarrier = np.linspace(1, nFFT, nFFT)
	S_00_real_coeff_6 = np.polyfit(nSubcarrier, S_imag[:, 0, 0], 6)	#Order 6 polynomial
	S_00_real_estimate_6 = np.poly1d(S_00_real_coeff_6) (nSubcarrier)
	S_00_real_coeff_4 = np.polyfit(nSubcarrier, S_imag[:, 0, 0], 4)	#Order 4 polynomial
	S_00_real_estimate_4 = np.poly1d(S_00_real_coeff_4) (nSubcarrier)
	
	plt.figure(2)
	plt.plot(S_real[:, 0, 0], label='Actual')
	plt.plot(S_00_real_estimate_6, label='Degree 6 Polynomial Approximation')
	plt.plot(S_00_real_estimate_4, label='Degree 4 Polynomial Approximation')
	plt.grid(linestyle='dotted')
	plt.legend()
	plt.title('Imaginary Part of S[1][0] after unwrapping')
	plt.show()
	'''
#----------------------------------------------------------------------
	np.savetxt(out, S_00_real_coeff_4, fmt='%.3e')
			

out.write("#Data Format" + "\n#If norm(A[k] - eye) > 1, V[k], V[k+1], A[k] are stored \n")	
for i in range(1):
	main()

out.close()
