import numpy as np
import matplotlib.pyplot as plt
import sys

nTx = 4;

H1 = np.random.randn(4,4);
U1, sigma1, Vh1 = np.linalg.svd(H1);
V1 = Vh1.conj().T;
sigma1 = [10, 2.001, 2, 0.1];
Sigma1 = np.diag(sigma1);
H1 = U1@Sigma1@Vh1;

H2 = H1 + 0.1*np.random.randn(4,4);
U2, sigma2, Vh2 = np.linalg.svd(H2);
V2 = Vh2.conj().T;
Sigma2 = np.diag(sigma2);
	
A = Vh1 @ V2;
swap = 0
swap1 = abs(A[0][1]) + abs(A[1][0])
swap2 = abs(A[1][2]) + abs(A[2][1])
swap3 = abs(A[2][3]) + abs(A[3][2])
Phi = np.zeros((nTx, nTx))
for j in range(nTx):
	swap += np.sum(abs(A[j]))		# Checks if offdiagonal elements are large
	swap -= abs(A[j][j])

Phi = np.eye(nTx, nTx);

if(abs(swap) > 1):
	if (abs(swap1) > 1):
		U_Phi, sigma_Phi, Vh_Phi = np.linalg.svd(A[0:2,0:2])
		Phi[0:2, 0:2] = (U_Phi @ Vh_Phi).conj().T
		print('Case 1')
	elif (abs(swap2) > 1):
		U_Phi, sigma_Phi, Vh_Phi = np.linalg.svd(A[1:3,1:3])
		Phi[1:3, 1:3] = (U_Phi @ Vh_Phi).conj().T
		print('Case 2')
	elif (abs(swap3) > 1):
		U_Phi, sigma_Phi, Vh_Phi = np.linalg.svd(A[2:,2:])
		Phi[2:, 2:] = (U_Phi @ Vh_Phi).conj().T
		print('Case 3')


V2 = V2 @ Phi
A2 = Vh1 @ V2;

print(A)
print(A2)