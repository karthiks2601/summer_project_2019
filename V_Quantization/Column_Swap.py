# Geodesic Interpolation

import numpy as np
import matplotlib.pyplot as plt
import sys

out = open("Data.txt", "w+")

def randn_c(nRx, nTx):
	return ((np.random.randn(nRx, nTx) + 1j* np.random.randn(nRx, nTx)) / np.sqrt(2))

arguments = sys.argv[1:]
count = len(arguments) // 2
if (count == 0):
	print('Please input the Power Delay Profile as Command Line Arguments with odd numbers ' \
			'indicating the index and even numbers indicating the magnitude')
	exit()

index = np.ones(count)
magnitude = np.ones(count)
for i in range(count):
	index[i] = arguments[2*i]
	magnitude[i] = arguments[2*i + 1]

nTx = 4
nRx = 4
nFFT = 64

#----------------------------------------------------------------------
def main():
	omega = np.linspace(0, 2*np.pi, nFFT)

	H = np.zeros((nFFT, nRx, nTx)) + 1j * np.zeros((nFFT, nRx, nTx))
	U = np.zeros((nFFT, nRx, nTx)) + 1j * np.zeros((nFFT, nRx, nTx))
	Vh = np.zeros((nFFT, nRx, nTx)) + 1j * np.zeros((nFFT, nRx, nTx))
	V =  np.zeros((nFFT, nRx, nTx)) + 1j * np.zeros((nFFT, nRx, nTx)) 
	sigma = np.zeros((nFFT, nTx))

	for i in range(count):
		temp = randn_c(nRx, nTx)
		for k in range(nFFT):
			H[k] += temp * np.exp(-1j * index[i] * omega[k]) * pow(10, magnitude[i] / 10)
			
	for k in range(nFFT):
		U[k], sigma[k], Vh[k] = np.linalg.svd(H[k])
		V[k] = Vh[k].conj().T		# Hermitian - .H is not available for ndarray
		

	#----------------------------------------------------------------------
	# To find if there is a swap in eigenvectors (find the permutation matrix Phi)
	# and find the Optimal Orientation Matrix
	Theta = np.zeros((nFFT - 1, nTx, nTx)) + 1j*np.zeros((nFFT - 1, nTx, nTx))		# Optimal Orientation Matrix is a diagonal matrix
	theta = np.zeros((nFFT - 1, nTx)) + 1j*np.zeros((nFFT - 1, nTx))		# theta contains the diagonal entries of Theta
	A = np.zeros((nFFT - 1, nRx, nTx)) + 1j * np.zeros((nFFT - 1, nRx, nTx))
	Delta = np.eye(nTx, nTx)
	S = np.zeros((nFFT, nTx, nTx)) + 1j * np.zeros((nFFT, nTx, nTx))

	S_00_real = np.zeros(nFFT)

	for k in range(nFFT - 1):
		A[k] = (V[k+1].conj().T) @ V[k]
		swap = 0
		swap1 = abs(A[k][0][1]) + abs(A[k][1][0])
		swap2 = abs(A[k][1][2]) + abs(A[k][2][1])
		swap3 = abs(A[k][2][3]) + abs(A[k][3][2])
		Phi = np.zeros((nTx, nTx))
		for j in range(nTx):
			swap += np.sum(abs(A[k][j]))		# Checks if offdiagonal elements are large
			swap -= abs(A[k][j][j])
		
		'''
		print(np.linalg.norm(A[k] - np.eye(nTx, nTx), 'fro'))		# To find out when there is a eigenvector swap
		if (np.linalg.norm(A[k] - np.eye(nTx, nTx), 'fro') > 1):
			out.write("#--------------------------------------------------------")
			out.write("\n")
			np.savetxt(out, V[k], fmt='%.3e')
			out.write("\n")
			np.savetxt(out, V[k+1], fmt='%.3e')
			out.write("\n")
			np.savetxt(out, A[k], fmt='%.3e')
			out.write("\n")
		'''
		
		if(abs(swap) > 1):
			print ('Column Swap Possible')
			out.write("#--------------------------------------------------------")
			out.write("\n")
			np.savetxt(out, V[k], fmt='%.3e')
			out.write("\n")
			np.savetxt(out, V[k+1], fmt='%.3e')
			out.write("\n")
			np.savetxt(out, A[k], fmt='%.3e')
			out.write("\n")
			
		#----------------------------------------------------------------------
			# If there is a column swap (even partial), there are two singular values close to each other.
			# In that case, if V is multiplied with a unitary matrix (with offdiagonal elements
			# only for the swapped singular values) hereafter called Delta, U can be multiplied with Delta
			# and there will be no change in H. We try to find such a Delta to minimize norm(V[k+1]-V[k]) 
			
			#U_Phi, sigma_Phi, Vh_Phi = np.linalg.svd(A[k])
			#Phi = U_Phi @ Vh_Phi 			# To find the closest Unitary Matrix to Phi (to multiply subsequent V)
			#Delta = Delta @ Phi
			Phi = np.eye(nTx, nTx) * (1 + 0j)
			if (abs(swap1) > 1):
				U_Phi, sigma_Phi, Vh_Phi = np.linalg.svd(A[k][0:2,0:2])
				Phi[0:2, 0:2] = (U_Phi @ Vh_Phi).conj().T
				print('Case 1')
			elif (abs(swap2) > 1):
				U_Phi, sigma_Phi, Vh_Phi = np.linalg.svd(A[k][1:3,1:3])
				Phi[1:3, 1:3] = (U_Phi @ Vh_Phi).conj().T
				print('Case 2')
			elif (abs(swap3) > 1):
				U_Phi, sigma_Phi, Vh_Phi = np.linalg.svd(A[k][2:,2:])
				Phi[2:, 2:] = (U_Phi @ Vh_Phi).conj().T
				print('Case 3')


			Delta = Delta @ Phi
			np.savetxt(out, Delta, fmt='%.3e')
			out.write("\n")
			
		V[k+1] = V[k+1] @ Delta

		A[k] = (V[k+1].conj().T) @ V[k]
		swap = 0
		for j in range(nTx):
			swap += np.sum(A[k][j])		# Checks if offdiagonal elements are large
			swap -= A[k][j][j]
		if(abs(swap) > 1):
			print('Impossible')
			np.savetxt(out, A[k], fmt='%.3e')
			out.write("\n")
		
	#----------------------------------------------------------------------
		
		for j in range(nTx):
			theta[k][j] = A[k][j][j] / abs(A[k][j][j])

		Theta[k] = np.diag(theta[k])
		V[k+1] = V[k+1] @ Theta[k]
		#print(np.linalg.norm(V[k] - V[k+1], 'fro'))		# To confirm that multiplication with Theta decreases the norm
		#print(np.linalg.norm(V[k] - V[k+1] @ Theta[k], 'fro'))
#-----------------------------------------------------------------------
	for k in range(nFFT):
		S[k] = np.log(V[k])
		S_00_real[k] = np.real(S[k][1][1])
		
	nSubcarrier = np.linspace(1, nFFT, nFFT)
	S_00_real_coeff_8 = np.polyfit(nSubcarrier, S_00_real, 8)	#Order 8 polynomial
	S_00_real_estimate_8 = np.poly1d(S_00_real_coeff_8) (nSubcarrier)
	S_00_real_coeff_4 = np.polyfit(nSubcarrier, S_00_real, 4)	#Order 4 polynomial
	S_00_real_estimate_4 = np.poly1d(S_00_real_coeff_4) (nSubcarrier)
	
	'''
	plt.plot(S_00_real, label='Actual')
	plt.plot(S_00_real_estimate_8, label='Degree 8 Polynomial Approximation')
	plt.plot(S_00_real_estimate_4, label='Degree 4 Polynomial Approximation')
	plt.grid(linestyle='dotted')
	plt.legend()
	plt.show()
	'''
#----------------------------------------------------------------------
out.write("#Data Format" + "\n#If norm(A[k] - eye) > 1, V[k], V[k+1], A[k] are stored \n")	
for i in range(100):
	main()

out.close()
