import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(1, 65)
y1 = 2 * x**3 + 3 * x**2 + 7 * x + 21 + 0.01 * x**4
coeff = np.polyfit(x, y1, 3)
y2 = np.poly1d(coeff) (x)
print(coeff)
plt.plot(x, y1)
plt.plot(x, y2)
plt.grid(linestyle = 'dotted')
plt.show()