import numpy as np
import matplotlib.pyplot as plt

f = np.loadtxt("MRC.txt")

plt.plot()
plt.semilogy(f[:,0],f[:,1], label ='Single Receiver')
plt.semilogy(f[:,0],f[:,2], label = 'Two Receivers')
plt.title('1X1 vs 1X2 - Maximal Ratio Combining')
plt.xlabel('SNR')
plt.ylabel('Bit Error Rate')
plt.grid(linestyle='dotted')

plt.legend()
plt.show()