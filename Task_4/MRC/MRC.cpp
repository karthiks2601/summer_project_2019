// 2 Rx - Maximal Ratio Combining
// Channel is generated every Tc symbols

#include <itpp/itcomm.h>
#include <fstream>
using namespace itpp;
using std::cout;
using std::endl;
int main()
{
	std::ofstream out("MRC.txt");

	int i, Number_of_bits, nTx, nRx, Tc;
	double Ec;
	vec SNR_dB, SNR, N0, noise_variance, bit_error_rate_1rx, bit_error_rate_2rx; 
	bvec transmitted_bits, received_bits;                 
	cvec transmitted_symbols, received_symbols_mrc;
	cmat channel_coeffs, received_symbols, rec_test, tx_test;
	
	//Classes
	BPSK_c bpsk;  //The BPSK modulator/debodulator class - for complex symbols
	AWGN_Channel awgn_channel;     //The AWGN channel class 
	BERC berc;  //The Bit Error Rate Counter class
	
	Tc = 100;
	nRx = 2;
	nTx = 1;
	Ec = 1.0;                      //The transmitted energy per QAM symbol is 1.
	SNR_dB = linspace(-20, 20.0, 21); //SNR is energy per received complex symbol time by energy of received noise per complex symbol time 
	SNR = inv_dB(SNR_dB);         
	N0 = pow(SNR, -1.0);     
	Number_of_bits = 100008; 
	channel_coeffs = ones_c(nRx, nTx);
	bit_error_rate_1rx.set_size(SNR_dB.length(), false);
	bit_error_rate_2rx.set_size(SNR_dB.length(), false);
	RNG_randomize();

	tx_test.set_size(nRx, Number_of_bits);
	rec_test.set_size(nRx, Number_of_bits);
	received_symbols.set_size(nRx, Number_of_bits);
	rec_test.set_size(nRx, Number_of_bits);
	received_symbols_mrc.set_size(Number_of_bits);
	
	for (i = 0; i < SNR_dB.length(); i++)
	{
		transmitted_bits = randb(Number_of_bits);
		transmitted_symbols = bpsk.modulate_bits(transmitted_bits);
		awgn_channel.set_noise(N0(i));

		for(int j = 0; j < Number_of_bits; j++)
		{
			if(j%Tc == 0)
			{
				channel_coeffs = randn_c(nRx, nTx);
			}

			tx_test(0, j) =  transmitted_symbols(j) * channel_coeffs(0, 0);
			tx_test(1, j) =  transmitted_symbols(j) * channel_coeffs(1, 0);
			
			received_symbols.set_col(j, awgn_channel(tx_test.get_col(j)));
			
			rec_test(0,j) = pow(channel_coeffs(0, 0), -1) * received_symbols(0, j);
			rec_test(1,j) = pow(channel_coeffs(1, 0), -1) * received_symbols(1, j);

			received_symbols_mrc(j) = conj(channel_coeffs(0, 0)) * received_symbols(0, j) + conj(channel_coeffs(1, 0)) * received_symbols(1, j);
			received_symbols_mrc(j) = received_symbols_mrc(j) / (pow(abs(channel_coeffs(0, 0)), 2) + pow(abs(channel_coeffs(1, 0)), 2));
			
		}

		
		received_bits = bpsk.demodulate_bits(rec_test.get_row(1));
		
		berc.clear();                               
		berc.count(transmitted_bits, received_bits);
		bit_error_rate_1rx(i) = berc.get_errorrate();
		
		received_bits = bpsk.demodulate_bits(received_symbols_mrc);
		//received_bits = bpsk.demodulate_bits((rec_test.get_row(1) + rec_test.get_row(0))*0.5);
		berc.clear();                               
		berc.count(transmitted_bits, received_bits);
		bit_error_rate_2rx(i) = berc.get_errorrate();
	}

	
	for (i = 0; i < SNR_dB.length(); i++)
	{
		out << SNR_dB[i] << "\t";
		out << bit_error_rate_1rx[i] << "\t";
		out << bit_error_rate_2rx[i] << "\t";
		out << inv_dB(SNR_dB[i]) << endl;
	}
	out.close();

	return 0;
}