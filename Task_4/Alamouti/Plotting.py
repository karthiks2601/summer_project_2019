import numpy as np
import matplotlib.pyplot as plt

def moving_average(seq):
	return np.convolve(seq, np.ones((3,)), mode='same')

f = np.loadtxt("Alamouti.txt")

#plt.semilogy(f[:,0],moving_average(f[:,1]), label ='Repetition Coding')
#plt.semilogy(f[:,0],moving_average(f[:,2]), label = 'Alamouti Scheme')
#plt.semilogy(f[:,0],moving_average(f[:,3]), label = 'SISO - With equal total power')

plt.semilogy(f[:,0],f[:,1], label ='Repetition Coding')
plt.semilogy(f[:,0],f[:,2], label = 'Alamouti Scheme')
plt.semilogy(f[:,0],f[:,3], label = 'SISO - With equal total power')

plt.title('2X1 - Repetition Coding vs Alamouti Scheme - QAM16')
plt.xlabel('SNR')
plt.ylabel('Bit Error Rate')
plt.grid(linestyle='dotted')

plt.legend()
plt.show()
