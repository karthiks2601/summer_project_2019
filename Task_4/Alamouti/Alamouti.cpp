// 2X1 - Alamouti Code

#include <itpp/itcomm.h>
#include <fstream>
using namespace itpp;
using std::cout;
using std::endl;
//----------------------------------------------------------------------
int main()
{
	std::ofstream out("Alamouti.txt");

	int i, Number_of_bits, nTx, nRx, Tc;
	double Ec;
	vec SNR_dB, SNR, N0, noise_variance, bit_error_rate_rep, bit_error_rate_ala, bit_error_rate_1x1; 
	bvec transmitted_bits, received_bits;                 
	cmat transmitted_symbols_rep, transmitted_symbols_ala, channel_coeffs, transmitted_symbols_1x1;
	cvec  received_symbols_rep, received_symbols_ala, received_symbols_1x1, rx_temp_rep, rx_temp_ala, rx_temp_1x1, tx_temp_rep, tx_temp_ala, tx_temp_1x1;
	
	//Classes
	QAM bpsk;
	int M = 16;
	bpsk.set_M(M);
	//BPSK_c bpsk;  //The BPSK modulator/debodulator class - for complex symbols
	AWGN_Channel awgn_channel;     //The AWGN channel class 
	BERC berc;  //The Bit Error Rate Counter class
	
	Tc = 100;
	nRx = 1;
	nTx = 2;
	Ec = 1.0;                      //The transmitted energy per QAM symbol is 1.
	SNR_dB = linspace(-20, 20.0, 81); //SNR is energy per received complex symbol time by energy of received noise per complex symbol time 
	SNR = inv_dB(SNR_dB);         
	N0 = pow(SNR, -1.0);     
	Number_of_bits = 100008; 
	channel_coeffs = ones_c(nRx, nTx);
	bit_error_rate_rep.set_size(SNR_dB.length(), false);
	bit_error_rate_ala.set_size(SNR_dB.length(), false);
	bit_error_rate_1x1.set_size(SNR_dB.length(), false);
	RNG_randomize();

//----------------------------------------------------------------------
// Set vector sizes
	transmitted_symbols_rep.set_size(nTx, Number_of_bits);
	transmitted_symbols_ala.set_size(nTx, Number_of_bits);
	transmitted_symbols_1x1.set_size(nTx, Number_of_bits);
	tx_temp_rep.set_size(Number_of_bits);
	tx_temp_ala.set_size(Number_of_bits);
	tx_temp_1x1.set_size(Number_of_bits);
	received_symbols_rep.set_size(Number_of_bits);
	received_symbols_ala.set_size(Number_of_bits);
	received_symbols_1x1.set_size(Number_of_bits);
	rx_temp_rep.set_size(Number_of_bits);
	rx_temp_ala.set_size(Number_of_bits);
	rx_temp_1x1.set_size(Number_of_bits);

//----------------------------------------------------------------------	
	for (i = 0; i < SNR_dB.length(); i++)
	{
		transmitted_bits = randb(Number_of_bits);
		transmitted_symbols_1x1.set_row(0, bpsk.modulate_bits(transmitted_bits));
		transmitted_symbols_1x1.set_row(1, bpsk.modulate_bits(transmitted_bits));
		transmitted_symbols_rep.set_row(0, bpsk.modulate_bits(transmitted_bits));
		transmitted_symbols_rep.set_row(1, transmitted_symbols_rep.get_row(0));

		transmitted_symbols_ala.set_row(0, transmitted_symbols_rep.get_row(0));

		awgn_channel.set_noise(N0(i));

		for(int j = 0; j < Number_of_bits; j++)
		{
			if(j%2 == 0)
			{
				transmitted_symbols_ala(1,j) = transmitted_symbols_ala(0, j+1);
			}
			else
			{
				transmitted_symbols_ala(0,j) = -conj(transmitted_symbols_ala(1, j-1));
				transmitted_symbols_ala(1,j) = conj(transmitted_symbols_ala(0, j-1));
			}
			if(j%Tc == 0)
			{
				channel_coeffs = randn_c(nRx, nTx);
			}

			tx_temp_1x1(j) =  transmitted_symbols_1x1(0, j) * channel_coeffs(0, 0);
			tx_temp_rep(j) =  transmitted_symbols_rep(0, j) * channel_coeffs(0, 0) + transmitted_symbols_rep(1, j) * channel_coeffs(0, 1);
			tx_temp_ala(j) =  transmitted_symbols_ala(0, j) * channel_coeffs(0, 0) + transmitted_symbols_ala(1, j) * channel_coeffs(0, 1);
			
			received_symbols_1x1(j) = tx_temp_1x1(j) + sqrt(N0(i)/2) * randn_c();
			received_symbols_rep(j) = tx_temp_rep(j) + sqrt(N0(i)) * randn_c();
			received_symbols_ala(j) = tx_temp_ala(j) + sqrt(N0(i)) * randn_c();

			rx_temp_1x1(j) = received_symbols_1x1(j) / (channel_coeffs(0, 0));
			rx_temp_rep(j) = received_symbols_rep(j) / (channel_coeffs(0, 0) + channel_coeffs(0, 1));
			
			if(j%2 != 0)
			{
				received_symbols_ala(j) = conj(received_symbols_ala(j));
				rx_temp_ala(j-1) = conj(channel_coeffs(0,0)) * received_symbols_ala(j-1) + channel_coeffs(0,1) * received_symbols_ala(j);
				rx_temp_ala(j) = conj(channel_coeffs(0,1)) * received_symbols_ala(j-1) - channel_coeffs(0,0) * received_symbols_ala(j);

				rx_temp_ala(j-1) = rx_temp_ala(j-1) / (pow(abs(channel_coeffs(0,0)), 2) + pow(abs(channel_coeffs(0,1)), 2));
				rx_temp_ala(j) = rx_temp_ala(j) / (pow(abs(channel_coeffs(0,0)), 2) + pow(abs(channel_coeffs(0,1)), 2));
			}

		}
	//----------------------------------------------------------------------
	// BER count	
		received_bits = bpsk.demodulate_bits(rx_temp_1x1);	
		berc.clear();                               
		berc.count(transmitted_bits, received_bits);
		bit_error_rate_1x1(i) = berc.get_errorrate();

		received_bits = bpsk.demodulate_bits(rx_temp_rep);	
		berc.clear();                               
		berc.count(transmitted_bits, received_bits);
		bit_error_rate_rep(i) = berc.get_errorrate();
		
		received_bits = bpsk.demodulate_bits(rx_temp_ala);
		berc.clear();                               
		berc.count(transmitted_bits, received_bits);
		bit_error_rate_ala(i) = berc.get_errorrate();
	}

	//----------------------------------------------------------------------
	// Output data
	for (i = 0; i < SNR_dB.length(); i++)
	{
		out << SNR_dB[i] << "\t";
		out << bit_error_rate_rep[i] << "\t";
		out << bit_error_rate_ala[i] << "\t";
		out << bit_error_rate_1x1[i] << "\t";
		out << inv_dB(SNR_dB[i]) << endl;
	}
	out.close();

	return 0;
}