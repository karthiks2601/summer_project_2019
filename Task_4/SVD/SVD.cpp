// SVD

#include <itpp/itcomm.h>
#include <fstream>
using namespace itpp;
using std::cout;
using std::endl;
//----------------------------------------------------------------------
int main()
{
	std::ofstream out("SVD.txt");

	int i, j, Number_of_bits, nTx, nRx, Tc;
	Tc = 100;
	Number_of_bits = 100008; 
	nRx = 4;
	nTx = 4;

	double Ec;
	vec SNR_dB, SNR, N0, noise_variance;
	mat bit_error_rate;
	Array<vec> lambda(Number_of_bits/Tc + 1); 
	bmat transmitted_bits, received_bits;
	cmat X, Y, Y_tilde, HX, X_tilde, X_hat;
	Array<cmat> H(Number_of_bits/Tc + 1), U(Number_of_bits/Tc + 1), V(Number_of_bits/Tc + 1);
	Array<mat> Lambda(Number_of_bits/Tc + 1);	//For Singular Value Decomposition of H
	
	//Classes
	BPSK_c bpsk;  //The BPSK modulator/debodulator class - for complex symbols
	AWGN_Channel awgn_channel;     //The AWGN channel class 
	BERC berc;  //The Bit Error Rate Counter class
	
	Ec = 1.0;                      //The net transmitted energy is 1 unit.
	SNR_dB = linspace(-20, 30.0, 26); //SNR is energy per received complex symbol time by energy of received noise per complex symbol time 
	SNR = inv_dB(SNR_dB);         
	N0 = pow(SNR, -1.0);     
	H = ones_c(nRx, nTx);
	RNG_randomize();

//----------------------------------------------------------------------
// Set vector sizes
	received_bits.set_size(nTx, Number_of_bits);
	bit_error_rate.set_size(SNR_dB.length(), nTx, false);
	X.set_size(nTx, Number_of_bits);
	HX.set_size(nRx, Number_of_bits);
	Y.set_size(nRx, Number_of_bits);
	Y_tilde.set_size(nRx, Number_of_bits);
	X_tilde.set_size(nTx, Number_of_bits);
	X_hat.set_size(nTx, Number_of_bits);

//----------------------------------------------------------------------	
	for (i = 0; i < SNR_dB.length(); i++)
	{
		awgn_channel.set_noise(N0(i));
		transmitted_bits = randb(nTx, Number_of_bits);

		for(j = 0; j < Number_of_bits; j++)
		{
			X.set_col(j, bpsk.modulate_bits(transmitted_bits.get_col(j)));	
		
			if(j%Tc == 0)
			{
				H(j/Tc) = randn_c(nRx, nTx);
				svd(H(j/Tc), U(j/Tc), lambda(j/Tc), V(j/Tc));
				diag(lambda(j/Tc), Lambda(j/Tc));
			}

			HX.set_col(j, H(j/Tc) * X.get_col(j));
			
			Y.set_col(j, awgn_channel(HX.get_col(j)));
			
			Y_tilde.set_col(j, hermitian_transpose(U(j/Tc)) * Y.get_col(j));
			X_tilde.set_col(j, (inv(Lambda(j/Tc)) * Y_tilde.get_col(j)).get_col(0));
			
			//X_hat.set_col(j, V(j/Tc) * X_tilde.get_col(j));
			X_hat.set_col(j, hermitian_transpose(V(j/Tc)) * X.get_col(j));

		}
	//----------------------------------------------------------------------
	// BER count
		for(j = 0; j < nTx; j++)
		{
			transmitted_bits.set_row(j, bpsk.demodulate_bits(X_hat.get_row(j)));	
			
			received_bits.set_row(j, bpsk.demodulate_bits(X_tilde.get_row(j)));	
			berc.clear();                               
			berc.count(transmitted_bits.get_row(j), received_bits.get_row(j));
			bit_error_rate(i, j) = berc.get_errorrate();
		}

	}

	//----------------------------------------------------------------------
	// Output data
	for (i = 0; i < SNR_dB.length(); i++)
	{
		out << SNR_dB[i] << "\t";
		for(j = 0; j < nTx; j++)
		{
			out << bit_error_rate(i, j) << "\t";
		}
		out << inv_dB(SNR_dB[i]) << endl;
	}
	out.close();

	return 0;
}