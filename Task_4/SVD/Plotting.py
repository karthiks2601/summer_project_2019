import numpy as np
import matplotlib.pyplot as plt

def moving_average(seq):
	return np.convolve(seq, np.ones((3,)), mode='same')

f = np.loadtxt("SVD.txt")
f2 = np.loadtxt("1x1.txt")

#plt.semilogy(f[:,0],moving_average(f[:,1]), label ='Repetition Coding')
#plt.semilogy(f[:,0],moving_average(f[:,2]), label = 'Alamouti Scheme')
#plt.semilogy(f[:,0],moving_average(f[:,3]), label = 'SISO - With equal total power')

nTx = 4
for  i in range(nTx):
	print(i)
	plt.semilogy(f[:,0],f[:,i+1])

plt.semilogy(f2[:,0], f2[:,1], label = 'SISO 1x1')

plt.title('BER vs SNR for 4X4 MIMO')
plt.xlabel('SNR')
plt.ylabel('Bit Error Rate')
plt.grid(linestyle='dotted')

plt.legend()
plt.show()
