import numpy as np
import matplotlib.pyplot as plt

def moving_average(seq):
	return np.convolve(seq, np.ones((3,)), mode='same')

f = np.loadtxt("Waterfilling.txt")

#plt.plot(f[:,0],f[:,1], label ='Waterfilling')
#plt.plot(f[:,0],f[:,2], label = 'Equal Power Allocation')
nDataPts = 41
#-------------------------------------------------------------------

'''
plt.plot(f[:nDataPts,0], f[:nDataPts,1], label='Waterfilling - 2x1')
plt.plot(f[nDataPts:2*nDataPts,0], f[nDataPts:2*nDataPts,1], label='Waterfilling - 4x1')
plt.plot(f[2*nDataPts:3*nDataPts,0], f[2*nDataPts:3*nDataPts,1], label='Waterfilling - 8x1')
plt.plot(f[3*nDataPts:4*nDataPts,0], f[3*nDataPts:4*nDataPts,1], label='Waterfilling - 16x1')

plt.plot(f[:nDataPts,0], f[:nDataPts,2], label='Eq Power Allocation - 2x1')
plt.plot(f[nDataPts:2*nDataPts,0], f[nDataPts:2*nDataPts,2], label='Eq Power Allocation - 4x1')
plt.plot(f[2*nDataPts:3*nDataPts,0], f[2*nDataPts:3*nDataPts,2], label='Eq Power Allocation - 8x1')
plt.plot(f[3*nDataPts:4*nDataPts,0], f[3*nDataPts:4*nDataPts,2], label='Eq Power Allocation - 16x1')

#-------------------------------------------------------------------
'''
plt.plot(f[:nDataPts,0], f[:nDataPts,1]/f[:nDataPts,2], label='2x1')
plt.plot(f[nDataPts:2*nDataPts,0], f[nDataPts:2*nDataPts,1]/f[nDataPts:2*nDataPts,2], label='4x1')
plt.plot(f[2*nDataPts:3*nDataPts,0], f[2*nDataPts:3*nDataPts,1]/f[2*nDataPts:3*nDataPts,2], label='8x1')
plt.plot(f[3*nDataPts:4*nDataPts,0], f[3*nDataPts:4*nDataPts,1]/f[3*nDataPts:4*nDataPts,2], label='16x1')


plt.title('Waterfilling vs Equal Power Allocation')
plt.xlabel('SNR')
#plt.ylabel('Capacity')
plt.ylabel('$C_{Waterfilling}$ / $C_{EqualPowerAllocation}$')
plt.grid(linestyle='dotted')

plt.legend()
plt.show()
