// Waterfilling

#include <itpp/itcomm.h>
#include <fstream>
using namespace itpp;
using std::cout;
using std::endl;
//----------------------------------------------------------------------
int main()
{
	std::ofstream out("Waterfilling.txt");

	int i, j, Number_of_bits, nRx, Tc;
	double Ec;
	vec SNR_dB, SNR, N0, noise_variance, power, C_waterfilling, C_eqpower, C_awgn, alpha, nTx;
	Array<vec> lambda(Number_of_bits/Tc + 1); 
	Array<cmat> H(Number_of_bits/Tc + 1), U(Number_of_bits/Tc + 1), V(Number_of_bits/Tc + 1);
	Array<mat> Lambda(Number_of_bits/Tc + 1);	//For Singular Value Decomposition of H
	
	Tc = 100;
	Number_of_bits = 40000; 
	nRx = 1;
	nTx.set("8");
	cout << nTx.length() << endl;
	//nTx.set("2 4 8 16");
	cout << nTx << endl;
	Ec = 1.0;                      //The net transmitted energy is 1 unit.
	SNR_dB = linspace(-20, 20.0, 41); //SNR is energy per received complex symbol time by energy of received noise per complex symbol time 
	SNR = inv_dB(SNR_dB);         
	N0 = pow(SNR, -1.0);     
	RNG_randomize();

//----------------------------------------------------------------------	
//Set vector sizes
	C_waterfilling = zeros(SNR.length());
	C_eqpower = zeros(SNR.length());
	C_awgn = zeros(SNR.length());	
//----------------------------------------------------------------------	
	for(int l = 0; l < nTx.length(); l++)
	{
		cout << "Test" << endl;
		alpha = zeros(nTx(l));
		power = zeros(nTx(l));
	
		for (i = 0; i < SNR_dB.length(); i++)
		{
			for(j = 0; j < Number_of_bits/Tc; j++)
			{
				H(j) = randn_c(nTx(l), nTx(l));
				svd(H(j), U(j), lambda(j), V(j));
				diag(lambda(j), Lambda(j));
			
				for(int k = 0; k < nTx(l); k++)
				{
					alpha(k) = pow(lambda(j).get(k), 2) * SNR(i);
				}
				power = waterfilling(alpha, Ec);
				
				for(int k = 0; k < nTx(l); k++)
				{
					C_waterfilling(i) += log2(1 + power(k) * alpha(k));
					C_eqpower(i) += log2(1 + alpha(k)/nTx(l));
				}
					C_awgn(i) += log2(1 + SNR(i));
			}
		}
		C_waterfilling = C_waterfilling * Tc / Number_of_bits;
		C_eqpower = C_eqpower * Tc / Number_of_bits;
		C_awgn = C_awgn * Tc / Number_of_bits;

		//----------------------------------------------------------------------
		// Output data
		for (i = 0; i < SNR_dB.length(); i++)
		{
			out << SNR_dB(i) << "\t";
			out << C_waterfilling(i) << "\t";
			out << C_eqpower(i) << "\t";
			out << C_awgn(i) << "\t";
			out << inv_dB(SNR_dB(i)) << endl;
		}
	}
	out.close();
	return 0;
}