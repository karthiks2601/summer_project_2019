import numpy as np
import matplotlib.pyplot as plt

f = np.loadfromtxt("QAM_AWGN_Compare.txt")
print(f)

plt.semilogy(f1[0,:],f1[1,:])

plt.xlabel('SNR in dB')
plt.ylabel('Bit Error Rate')
plt.title('BER vs SNR for BPSK: Fading vs AWGN')
plt.grid()
plt.legend()
plt.show()