//QAM-64 over an AWGN Channel

#include <itpp/itcomm.h>
#include <fstream>
using namespace itpp;
using std::cout;
using std::endl;
int main()
{
	std::ofstream out("QAM_AWGN_Compare.txt");

	int i, Number_of_bits;
	double Ec;
	vec SNR_dB, SNR, N0, noise_variance, bit_error_rate; 
	bvec transmitted_bits, received_bits;                 
	cvec transmitted_symbols, received_symbols;           
	
	//Declarations of classes:
	ivec M = "4 16 64 256";
	QAM qam64;						//QAM modulator class
	AWGN_Channel awgn_channel;     //The AWGN channel class                 
	BERC berc;                     //Used to count the bit errors
	
	//Initialization:
	Ec = 1.0;                      //The transmitted energy per QAM symbol is 1.
	SNR_dB = linspace(0.0, 30.0, 16); //SNR is energy per received complex symbol time by energy of received noise per complex symbol time 
	SNR = inv_dB(SNR_dB);         
	N0 = pow(SNR, -1.0);     
	Number_of_bits = 100008;       

	bit_error_rate.set_size(SNR_dB.length(), false);
	RNG_randomize();

	for (int j = 0; j < M.length(); j++)
	{

		qam64.set_M(M[j]);
		
		for (i = 0; i < SNR_dB.length(); i++)
		{
			
			transmitted_bits = randb(Number_of_bits);
			transmitted_symbols = qam64.modulate_bits(transmitted_bits);
			awgn_channel.set_noise(N0(i));
			received_symbols = awgn_channel(transmitted_symbols);
			
			received_bits = qam64.demodulate_bits(received_symbols);
			berc.clear();                               
			berc.count(transmitted_bits, received_bits);
			bit_error_rate(i) = berc.get_errorrate();   
		}
		
		//Print the results:
		cout << "SNR_dB = " << SNR_dB << " [dB]" << endl;
		cout << "BER = " << bit_error_rate << endl;
		cout << "Saving results to QAM_AWGN_Compare.txt" << endl;
		cout << endl;	
	
		out << SNR_dB << endl;
		out << bit_error_rate << endl;
	}
	out.close();
	return 0;
}
